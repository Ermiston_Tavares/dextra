package com.fastnews.service.api

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

abstract class RedditAPI {

    companion object {

        private lateinit var REDDIT_API_SERVICE: RedditService

        fun getService(context: Context): RedditService {




            if (!Companion::REDDIT_API_SERVICE.isInitialized) {


                val cacheSize = 10 * 1024 * 1024 // 10 MB
                val httpCacheDirectory = File(context.getCacheDir(), "http-cache")
                val cache = Cache(httpCacheDirectory, cacheSize.toLong())

                val networkCacheInterceptor = Interceptor { chain ->
                    val response = chain.proceed(chain.request())

                    var cacheControl = CacheControl.Builder()
                        .maxAge(1, TimeUnit.MINUTES)
                        .build()

                    response.newBuilder()
                        .header("Cache-Control", cacheControl.toString())
                        .build()
                }

                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY


            var API_BASE_URL: String = "https://www.reddit.com/r/Android/"
            var httpClient = OkHttpClient.Builder()
                .cache(cache) // 10 MB
                .addNetworkInterceptor(networkCacheInterceptor)
                .addInterceptor(loggingInterceptor)

            var builder: Retrofit.Builder = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())

            var retrofit = builder
                .client(httpClient.build())
                .build()

                REDDIT_API_SERVICE = retrofit.create<RedditService>(
                    RedditService::class.java
                )


        }

            return REDDIT_API_SERVICE
        }

    }




}