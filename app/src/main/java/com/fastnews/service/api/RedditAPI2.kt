package com.fastnews.service.api

import android.content.Context
import android.util.Log
import com.fastnews.mechanism.VerifyNetworkInfo
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

abstract class RedditAPI2 {


    companion object {

        private lateinit var redditApiService: RedditService
        private var mCache: Cache? = null

        var API_BASE_URL: String = "https://www.reddit.com/r/Android/"
        //const val HEADER_CACHE_CONTROL = "Cache-Control"
        //const val HEADER_PRAGMA = "Pragma"

        fun getService(context: Context): RedditService {

            if (!Companion::redditApiService.isInitialized) {


               /* var httpClient = OkHttpClient.Builder()
                    .addInterceptor(provideOfflineCacheInterceptor(context))
                    .addNetworkInterceptor(provideCacheInterceptor())
                    .cache(provideCache());

                var builder: Retrofit.Builder = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())

                var retrofit = builder
                    .client(httpClient.build())
                    .build()*/
                //var retrofit = getCachedRetrofit(context)

                redditApiService = getCachedRetrofit(context)?.create<RedditService>(
                    RedditService::class.java
                )!!

            }

            return redditApiService

        }

        private fun getCachedRetrofit(context: Context): Retrofit? {
            //if (mCachedRetrofit == null) {
                val httpClient =
                    OkHttpClient.Builder() // Add all interceptors you want (headers, URL, logging)
                        .addInterceptor(httpLoggingInterceptor())
                        .addInterceptor(provideOfflineCacheInterceptor())
                        .addNetworkInterceptor(provideCacheInterceptor(context))
                        .cache(provideCache(context))

            //mCachedOkHttpClient = httpClient.build()
                val retrofit = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .client(httpClient.build())
                    .build()
          //  }
            return retrofit
        }

        private fun httpLoggingInterceptor() : HttpLoggingInterceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            return loggingInterceptor
        }

        private fun provideCache(context: Context): Cache? {
            if (mCache == null) {
                try {
                    if (context != null) {
                        mCache = Cache(
                            File(context.cacheDir, "http-cache"),
                            10 * 1024 * 1024
                        )
                    } // 10 MB
                } catch (e: Exception) {
                    Log.e("", "Could not create Cache!")
                }
            }
            return mCache
        }

        private fun provideCacheInterceptor(context: Context): Interceptor? {
            return Interceptor { chain: Interceptor.Chain ->

                /*val response = chain.proceed(chain.request())

                var cacheControl = CacheControl.Builder()
                    .maxAge(1, TimeUnit.MINUTES)
                    .build()

                response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()*/

                val response = chain.proceed(chain.request())
                val cacheControl: CacheControl = if (VerifyNetworkInfo.isConnected(context)) {
                    CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build()
                } else {
                    CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()
                }
                /*response.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build()*/

                response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()



                /*var request = chain.request()
                val originalResponse = chain.proceed(request)
                val cacheControl = originalResponse.header("Cache-Control")

                if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains(
                        "no-cache"
                    ) ||
                    cacheControl.contains("must-revalidate") || cacheControl.contains("max-stale=0")
                ) {
                    val cc = CacheControl.Builder()
                        .maxStale(1, TimeUnit.DAYS)
                        .build()

                    request = request.newBuilder()
                        .cacheControl(cc)
                        .build()
                    chain.proceed(request)
                } else {
                    originalResponse
                }*/
            }
        }

        private fun provideOfflineCacheInterceptor(): Interceptor? {
            return Interceptor { chain: Interceptor.Chain ->
                /*var request: Request = chain.request()
                if (!isConnected(context)) {
                    val cacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()
                    request = request.newBuilder()
                        .removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build()
                }
                chain.proceed(request)*/
                try {
                    chain.proceed(chain.request())
                } catch (e: java.lang.Exception) {
                    val cacheControl = CacheControl.Builder()
                        .onlyIfCached()
                        .maxStale(1, TimeUnit.DAYS)
                        .build()
                    val offlineRequest = chain.request().newBuilder()
                        .cacheControl(cacheControl)
                        .build()
                    chain.proceed(offlineRequest)
                }
            }
        }

        /*private fun provideForcedOfflineCacheInterceptor(): Interceptor? {
            return Interceptor { chain: Interceptor.Chain ->
                var request: Request = chain.request()
                val cacheControl = CacheControl.Builder()
                    .maxStale(7, TimeUnit.DAYS)
                    .build()
                request = request.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .cacheControl(cacheControl)
                    .build()
                chain.proceed(request)
            }
        }*/

        /*fun clean() {
            if (mOkHttpClient != null) {
                // Cancel Pending Request
                mOkHttpClient?.dispatcher()?.cancelAll()
            }
            if (mCachedOkHttpClient != null) {
                // Cancel Pending Cached Request
                mCachedOkHttpClient?.dispatcher()?.cancelAll()
            }
            mRetrofit = null
            mCachedRetrofit = null
            if (mCache != null) {
                try {
                    mCache!!.evictAll()
                } catch (e: IOException) {
                    Log.e("", "Error cleaning http cache")
                }
            }
            mCache = null
        }*/

        /*private fun isConnected(context: Context): Boolean {
            try {
                val e = context?.getSystemService(
                    Context.CONNECTIVITY_SERVICE
                ) as ConnectivityManager
                val activeNetwork = e.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnectedOrConnecting
            } catch (e: Exception) {
                Log.w("", e.toString())
            }
            return false
        }*/

    }





}


