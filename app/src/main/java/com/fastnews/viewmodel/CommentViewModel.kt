package com.fastnews.viewmodel

import android.app.Application
import androidx.annotation.UiThread
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fastnews.mechanism.Coroutines
import com.fastnews.repository.CommentRepository
import com.fastnews.service.model.CommentData

class CommentViewModel(application: Application) : AndroidViewModel(application) {

    private var mComments = MutableLiveData<List<CommentData>>()
    private val mContext = application.applicationContext

    @UiThread
    fun getComments(postId: String): LiveData<List<CommentData>> {
        Coroutines.ioThenMain({
            CommentRepository.getComments(postId, mContext)
        }) {
            mComments.postValue(it)
        }

        return mComments
    }

}